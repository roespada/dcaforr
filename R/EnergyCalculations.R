###################################################################################
# Funciones para calculos energeticos a partir de DCA / DCAid
###################################################################################
### Modificacion: 
### Calculo CORRECTO de energias locales a partir del modelo de Potts de DCA
# fmarg: matriz de frecuencias marginales
# eij: matriz de parametros eij
# out.aa : aminoacido excluido de los calculos de eij
# npos: largo de secuencias analizadas
# naa: cantidad de aminoacidos condiderados
## Salida: lista con dos elementos: matriz de parametros h / matriz de parametros h balanceados
local.fields2<-function(fmarg,eij,out.aa="-",npos=nrow(fmarg),naa=21){
  
  if(is.character(out.aa)){out.aa<-msa2num(out.aa)}
  
  #CALCULO CAMPOS LOCALES hi(a)
  #matriz de eij por pj
  vfmarg<-c(t(fmarg))   #armo un vector de frecuencias marginales
  
  #Calculo la matriz h
  h<-matrix(nrow=npos,ncol=naa)
  for(i in 1:npos){
    #miro que columnas corresponden a la posicion i
    columnas.posicion.i<- ((naa)*(i-1)+1):((naa)*i)
    #cuenta de paper sin sacar los valores del aa referencia, porq son todos cero...:
    h[i,]<-log(fmarg[i,]/fmarg[i,out.aa])-
      t(apply(vfmarg[-columnas.posicion.i]*t(eij[columnas.posicion.i,-columnas.posicion.i]),2,sum))
  }
  
  ## BALANCEO LA MATRIZ h
  hc<-matrix(ncol=ncol(h),nrow=nrow(h))
  t1<-apply(h,1,sum)
  for(i in 1:npos){
    for(a in 1:naa){
      cinvrow<- (i-1)*naa+a
      columnas.posicion.i<- ((naa)*(i-1)+1):((naa)*i)
      hc[i,a]<-h[i,a]-1/naa*t1[i]+0.5*(                                  # ESTE 0.5 ES LA MODIFICACION
        1/naa*sum(eij[cinvrow,])-1/naa^2*sum(eij[columnas.posicion.i,])+
          1/naa*sum(eij[,cinvrow])-1/naa^2*sum(eij[,columnas.posicion.i])
      )
    }
  }
  
  colnames(h)<-colnames(hc)<-colnames(fmarg)
  return(list(h=h,h.balanced=hc))
  
}


### Poner ceros en las matrices eij
# poner ceros en los bloques diagonales de dimension naa*npos en la matriz m
eij.null.diag<-function(m,naa=21,npos){
  if(ncol(m)!=(naa*npos) | nrow(m)!=(naa*npos)){stop('Wrong dimension')}
  
  for(i in 1:npos){
    ipos<-(naa*(i-1)+1):(naa*(i))
    m[ipos,ipos]<-0
  }
  return(m)
}


### Ademas, en los calculos de energias es necesario fijar los valores Jii (o eii)=0.
# lf2<-local.fields2(fmarg = DCAid$fmc,eij = DCAid$cf$eij-DCAid$cf$Cdiag)

## Asignar energia a una secuencia a partir de las matrices de energias locales y couplings
#seq: sequence
#local: matrix of local energies
#coupling: matrix of coupling energies
#naa: number of aminoacids, 21 by default
#l: length sequence / of the msa
energy.one.seq<-function(seq,local,coupling,naa=21,npos=NULL){
    if(is.null(npos)){npos<-length(seq)}
    pos.vector<-((naa)*(c(1:npos)-1))
    
    local.energies<-sum(local[cbind(1:npos,seq)])
    
    #interaction energies
    index<-pos.vector+seq
    interact.energies<-sum(coupling[index,index])/2
    return(c(local.energies,interact.energies,local.energies+interact.energies))
}

energy.for.seq<-function(msa,local,coupling,naa=NULL,npos=NULL){
    
    if(is.null(npos)){npos<-nrow(local)
                      warning("npos set as nrow(fmarg)")}
    if(is.null(naa)){naa<-ncol(local)
                     warning("naa set as ncol(fmarg)")}
    
    e3<-t(apply(msa,1,energy.one.seq,local=local,coupling=coupling,naa=naa,npos=npos))
    
    if(!is.null(rownames(msa))){rownames(energy<-rownames(msa))}
    colnames(e3)<-c("local","interact","total")
    
    return(e3)
}

